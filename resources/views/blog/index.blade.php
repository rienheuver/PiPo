@extends('layouts.onepart')
@section('title','Blog')

@section('content')
<div class="section">
    <div class="container">
        @if (Auth::user())
            <a href="{{ action('BlogController@create') }}" class="btn">New blogpost</a>
        @endif
        @foreach ($posts as $post)
            <div class="row">
                <div class="col s12">
                    <div class="card">
                        <div class="card-content">
                            <span class="card-title">{{ $post->title }}</span>
                            <p>{{ $post->content }}</p>
                        </div>
                        <div class="card-action">
                            <form method="post" action="{{ action('BlogController@destroy') }}">
                                {{ csrf_field() }}
                                {{ method_destroy() }} // TODO: zoiets?
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
@endsection
