@extends('layouts.onepart')
@section('title', 'New blogpost')

@section('content')
<div class="container">
    <div class="row">
        <div class="col s12">
            <h2>New blogpost</h2>
            <form method="post" action="{{ action('BlogController@store') }}">
                {{ csrf_field() }}
                <div class="row">
                    <div class="input-field col s6">
                        <input id="title" type="text" name="title" class="validate">
                        <label for="title">Title</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <textarea id="content" name="content" class="materialize-textarea"></textarea>
                        <label for="content">Content</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12">
                        <button type="submit" class="btn">Create blogpost</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
