@extends('layouts.basic')

@section('main')
<div class="row">
    <div class="col s12">
        @yield('content')
    </div>
</div>
@endsection

@push('js')
    @include('errors.display')
@endpush
