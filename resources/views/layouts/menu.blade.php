<div class="navbar-fixed">
    <nav>
        <div class="nav-wrapper container">
            <a href="{{ url('/') }}" class="brand-logo">PiPo</a>
            <a href="#" data-activates="mobile-menu" class="button-collapse"><i class="material-icons">menu</i></a>
            <ul class="right hide-on-med-and-down">
                <li><a href="#story">Story</a></li>
                <li><a href="#team">Team</a></li>
                <li><a href="#whitepaper">Whitepaper</a></li>
                <li><a href="{{ action('BlogController@index') }}">Blog</a></li>
                @if (Auth::user())
                    <li><a href="{{ action('Auth\LoginController@logout') }}"
                       onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                        Logout
                    </a></li>
                    <form id="logout-form" action="{{ action('Auth\LoginController@logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                @else
                    <li><a href="{{ action('Auth\LoginController@login') }}">Login</a></li>
                @endif
            </ul>
        </div>
    </nav>
</div>
