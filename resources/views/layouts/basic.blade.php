<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }} - @yield('title')</title>

    <!-- Styles -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>

    @include('layouts.menu')
        <div id="content">
            @yield('main')
        </div>

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}"></script>
    <script type="text/javascript">
        $(function() {
            $(".button-collapse").sideNav();
            $(".dropdown-button").dropdown();
            @if (session('status'))
                Materialize.toast('{{ session("status") }}');
            @endif
            @if (session('status-error'))
                Materialize.toast('{{ session("status-error") }}');
            @endif
            @if (session('status-success'))
                Materialize.toast('{{ session("status-success") }}');
            @endif
            @if (session('status-info'))
                Materialize.toast('{{ session("status-info") }}');
            @endif
            @if (session('status-warning'))
                Materialize.toast('{{ session("status-warning") }}');
            @endif
        });
    </script>
    @stack('js')
    @yield('js')
</body>
</html>
