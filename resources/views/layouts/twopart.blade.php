@extends('layouts.basic')

@section('main')
<div class="row">
    <div class="col s3">
        @yield('sidemenu')
    </div>

    <div class="col s9">
        @include('errors.display')
        @yield('content')
    </div>
</div>
@endsection
