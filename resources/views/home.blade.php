@extends('layouts.onepart')

@section('title', 'Homepage')

@section('content')
<div class="section no-pad-bot" id="index-banner">
  <div class="container">
    <br><br>
    <h1 class="header center orange-text">PieterPoints</h1>
    <div class="row center">
      <h5 class="header col s12 light">A good one-liner about PieterPoints</h5>
    </div>
    <div class="row center">
      <a href="#team" id="download-button" class="btn-large waves-effect waves-light orange">Team</a>
    </div>
    <br><br>

  </div>
</div>


<div class="container" id="story">
  <div class="section">

    <!--   Icon Section   -->
    <div class="row">
      <div class="col s12 m4">
        <div class="icon-block">
          <h2 class="center light-blue-text"><i class="material-icons">attach_money</i></h2>
          <h5 class="center">Reason 1</h5>

          <p class="light">Blablabla</p>
        </div>
      </div>

      <div class="col s12 m4">
        <div class="icon-block">
          <h2 class="center light-blue-text"><i class="material-icons">group</i></h2>
          <h5 class="center">Reason 2</h5>

          <p class="light">Blablabla</p>
        </div>
      </div>

      <div class="col s12 m4">
        <div class="icon-block">
          <h2 class="center light-blue-text"><i class="material-icons">autorenew</i></h2>
          <h5 class="center">Reason 3</h5>

          <p class="light">Blablabla</p>
        </div>
      </div>
    </div>

  </div>
  <br><br>
</div>

<div class="section light-blue" id="team">
    <div class="container">
        <div class="row">
            <div class="col s12">
                <h3 class="white-text">Our team</h3>
                <img class="circle" src="{{ asset('images/Team.jpg') }}">
            </div>
            <div class="col s4">
                <img class="circle" src="{{ asset('images/Hans.jpg') }}">
                <h4 class="white-text">Hans</h4>
                <p class="grey-text text-lighten-4"></p>
            </div>
            <div class="col s4">
                <img class="circle" src="{{ asset('images/Harm.jpg') }}">
                <h4 class="white-text">Harm</h4>
                <p class="grey-text text-lighten-4">"Ever since I was born, I knew I was destined for something greater. All my life I have been trying to find my purpose. Once I found out about this venture, I knew my time had come. It is clear to me that this revolutionary technology will change the way people will think and act. Whether it is in small companies, giant companies, government or ordinary family life, this technology is usable. It is adaptable to every situation. Together we will change the world, but we will not stop there. The technology is so revolutionary that we can take this further. Changing the world once is nice, but we would like to achieve a continious chain of world changing events. Kind of like a world changing event blockchain. Because the history of this chain is preserved, we can always find what the current state of affairs are. This way, we will make newspapers obsolete. But we will not stop there. Because of the seamless sharing of the news worldwide, all countries feel more and more equal. Therefore there will be no need for borders anymore. At a certain point in the distant future, the chain will be the only thing keeping us going. This technology will help us to revolutionize the world for the future." - Harm, 2017</p>
            </div>
            <div class="col s4">
                <img class="circle" src="{{ asset('images/Hugo.jpg') }}">
                <h4 class="white-text">Hugo</h4>
                <p class="grey-text text-lighten-4">Hugo was fascinated by the idea of crypto currencies for quite a while. The reason he never dived into the domain before was the lack of trust in the exciting coins. That all changes now that he can start the adventure with his fellow students.</p>
            </div>
        </div>
        <div class="row">
            <div class="col s4">
                <img class="circle" src="{{ asset('images/Janina.jpg') }}">
                <h4 class="white-text">Janina</h4>
                <p class="grey-text text-lighten-4">Having undergone the concept of PieterPoints before, she immediately saw the value this currency holds. She is intently happy to have found a group of enthusiast, motivated and skilled people to bring the concept to life. </p>
            </div>
            <div class="col s4">
                <img class="circle" src="{{ asset('images/Rien.jpg') }}">
                <h4 class="white-text">Rien</h4>
                <p class="grey-text text-lighten-4">He does not really care, he is just in it for the fun.</p>
            </div>
            <div class="col s4">
                <img class="circle" src="{{ asset('images/Tim.jpg') }}">
                <h4 class="white-text">Tim</h4>
                <p class="grey-text text-lighten-4">He has been searching for a crypto currency to invest in for a long time. Unfortunately, none of the exciting possibilities satisfied his needs. He joined the PieterPoints project to create a truly worthy currency!</p>
            </div>
        </div>
    </div>
</div>



<footer class="page-footer orange" id="whitepaper">
  <div class="container">
    <div class="row">
      <div class="col l6 s12">
        <h3 class="white-text">White paper</h3>
        <p class="grey-text text-lighten-4">We are a team of college students working on this project like it's our full time job. Any amount would help support and continue development on this project and is greatly appreciated.</p>
      </div>
    </div>
  </div>
  <div class="footer-copyright">
    <div class="container">
    Made by <a class="orange-text text-lighten-3" href="#">4TU</a>
    </div>
  </div>
</footer>
@endsection
