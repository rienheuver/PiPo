@extends('layouts.onepart')
@section('title','Login')

@section('content')
<div class="container">
    <div class="row">
        <div class="col s12">
            <div class="card">
                <form method="POST" action="{{ route('login') }}">
                    <div class="card-content">
                        {{ csrf_field() }}

                        <div class="row">
                            <div class="input-field col s12">
                                <label for="email">E-mail</label>
                                <input id="email" type="email" class="validate" name="email" value="{{ old('email') }}" required>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12">
                                <label for="password" class="col-md-4 control-label">Password</label>
                                <input id="password" type="password" class="form-control" name="password" required>
                            </div>
                        </div>

                        <div class="row">
                            <div class="cols12">
                                <input type="checkbox" id="remember" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                <label for="remember">Remember Me</label>
                            </div>
                        </div>
                    </div>
                    <div class="card-action">
                        <button type="submit" class="btn">
                            Login
                        </button>
                        <a class="btn" href="{{ route('password.request') }}">
                            Forgot Your Password?
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
