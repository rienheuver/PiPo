@extends('layouts.basic')

@section('title','Page not found')

@section('main')
<div class="row">
    <div class="col s12">
        <div class="card-panel blue">
            <span class="white-text">
                This page doesn't seem to exist... so what are you doing here?
            </span>
        </div>
    </div>
</div>
@endsection
