@extends('layouts.basic')

@section('title','Service unavailable')

@section('main')
<div class="row">
    <div class="col s12">
        <div class="card-panel blue">
            <span class="white-text">
                Oops, we appear to be down. Find us at <a href="mailto:root@devvers.work">root@devvers.work</a>!
            </span>
        </div>
    </div>
</div>
@endsection
