@if ($errors->count() > 0)
    <script>
        @foreach ($errors->all() as $error)
            Materialize.toast('{{ $error }}',2000);
        @endforeach
    </script>
@endif
