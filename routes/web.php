<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/blog', 'BlogController@index');

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::get('/blog/create', 'BlogController@create');
    Route::post('/blog/create', 'BlogController@store');
});
